import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.client.*
import io.ktor.client.engine.apache.*

import io.ktor.client.engine.jetty.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.auth.*
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.websocket.*
import io.ktor.http.cio.websocket.*
import java.time.*
import io.ktor.client.features.websocket.*
import io.ktor.client.features.websocket.WebSockets
import io.ktor.http.cio.websocket.Frame
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import io.ktor.client.features.logging.*
import io.ktor.auth.*
import io.ktor.sessions.*
import io.ktor.features.*
import org.slf4j.event.*
import java.io.*
import java.util.*
import io.ktor.network.selector.*
import io.ktor.network.sockets.*
import io.ktor.network.util.*
import io.ktor.server.cio.CIO
import io.ktor.util.KtorExperimentalAPI

import kotlin.coroutines.*
import io.ktor.utils.io.*

import io.ktor.utils.io.core.*
import kotlinx.coroutines.*


object WsClientApp {
    @KtorExperimentalAPI
    @JvmStatic
    fun main(args: Array<String>) {
        runBlocking {
            val client = HttpClient(io.ktor.client.engine.cio.CIO).config { install(WebSockets) }
            client.ws(method = HttpMethod.Get, host = "127.0.0.1", port = 8080, path = "/myws/echo") {
                send(Frame.Text("Hello World"))
                for (message in incoming) {
                    if (message is Frame.Text) {
                        println("Server said: " + message.readText())
                    }

                }
            }
        }
    }
}
